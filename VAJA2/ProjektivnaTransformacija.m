slika = imread('Image_rect1.tif');

slika = repmat(slika, [1, 1, 3]);
image(uint8(slika));

disp(size(slika));

[u, v] = ginput(4);


T = maketform('projective',[u,v], [0 0; 0 300; 300 300; 300 0]);

disp(u);
disp(v);

I2 = imtransform(slika,T, 'Xdata', [0, 300], 'Ydata', [0, 300]);


figure, imshow(I2);