% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 658.016900024273923 ; 658.658019008848100 ];

%-- Principal point:
cc = [ 303.172204493682671 ; 248.182866625928426 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.258529506703038 ; 0.145759654602265 ; 0.000870763640021 ; -0.000164560023345 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 1.704428659701452 ; 1.232444728497163 ];

%-- Principal point uncertainty:
cc_error = [ 1.719532519915485 ; 2.617947730285282 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.007184275276036 ; 0.034251255161098 ; 0.000776433098230 ; 0.000387261466094 ; 0.000000000000000 ];

%-- Image size:
nx = 640;
ny = 480;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.660438e+00 ; 1.656263e+00 ; -6.642738e-01 ];
Tc_1  = [ -1.778183e+02 ; -9.092905e+01 ; 8.528435e+02 ];
omc_error_1 = [ 2.329490e-03 ; 2.482830e-03 ; 3.304541e-03 ];
Tc_error_1  = [ 2.229182e+00 ; 3.291077e+00 ; 2.213972e+00 ];

%-- Image #2:
omc_2 = [ 2.611117e+00 ; 3.489883e-02 ; 1.607307e-01 ];
Tc_2  = [ -1.532226e+02 ; 1.196683e+02 ; 5.913329e+02 ];
omc_error_2 = [ 2.654759e-03 ; 1.045918e-03 ; 3.578800e-03 ];
Tc_error_2  = [ 1.563803e+00 ; 2.502043e+00 ; 1.619337e+00 ];

%-- Image #3:
omc_3 = [ 2.609114e+00 ; 2.282842e-01 ; 2.116367e-02 ];
Tc_3  = [ -1.776236e+02 ; 1.001622e+02 ; 6.102713e+02 ];
omc_error_3 = [ 2.613463e-03 ; 9.702258e-04 ; 3.534375e-03 ];
Tc_error_3  = [ 1.612843e+00 ; 2.558527e+00 ; 1.695461e+00 ];

%-- Image #4:
omc_4 = [ 2.456603e+00 ; 1.813845e-01 ; -5.590652e-01 ];
Tc_4  = [ -1.487096e+02 ; 1.049488e+02 ; 6.024939e+02 ];
omc_error_4 = [ 3.061490e-03 ; 1.610667e-03 ; 3.526766e-03 ];
Tc_error_4  = [ 1.582481e+00 ; 2.486900e+00 ; 1.364999e+00 ];

%-- Image #5:
omc_5 = [ 1.084548e+00 ; 1.924819e+00 ; -2.450327e-01 ];
Tc_5  = [ -9.246381e+01 ; -2.353890e+02 ; 7.353883e+02 ];
omc_error_5 = [ 2.860340e-03 ; 2.653294e-03 ; 3.145922e-03 ];
Tc_error_5  = [ 1.951632e+00 ; 2.784892e+00 ; 1.901631e+00 ];

%-- Image #6:
omc_6 = [ NaN ; NaN ; NaN ];
Tc_6  = [ NaN ; NaN ; NaN ];
omc_error_6 = [ NaN ; NaN ; NaN ];
Tc_error_6  = [ NaN ; NaN ; NaN ];

%-- Image #7:
omc_7 = [ NaN ; NaN ; NaN ];
Tc_7  = [ NaN ; NaN ; NaN ];
omc_error_7 = [ NaN ; NaN ; NaN ];
Tc_error_7  = [ NaN ; NaN ; NaN ];

%-- Image #8:
omc_8 = [ NaN ; NaN ; NaN ];
Tc_8  = [ NaN ; NaN ; NaN ];
omc_error_8 = [ NaN ; NaN ; NaN ];
Tc_error_8  = [ NaN ; NaN ; NaN ];

%-- Image #9:
omc_9 = [ NaN ; NaN ; NaN ];
Tc_9  = [ NaN ; NaN ; NaN ];
omc_error_9 = [ NaN ; NaN ; NaN ];
Tc_error_9  = [ NaN ; NaN ; NaN ];

%-- Image #10:
omc_10 = [ NaN ; NaN ; NaN ];
Tc_10  = [ NaN ; NaN ; NaN ];
omc_error_10 = [ NaN ; NaN ; NaN ];
Tc_error_10  = [ NaN ; NaN ; NaN ];

%-- Image #11:
omc_11 = [ NaN ; NaN ; NaN ];
Tc_11  = [ NaN ; NaN ; NaN ];
omc_error_11 = [ NaN ; NaN ; NaN ];
Tc_error_11  = [ NaN ; NaN ; NaN ];

%-- Image #12:
omc_12 = [ NaN ; NaN ; NaN ];
Tc_12  = [ NaN ; NaN ; NaN ];
omc_error_12 = [ NaN ; NaN ; NaN ];
Tc_error_12  = [ NaN ; NaN ; NaN ];

%-- Image #13:
omc_13 = [ NaN ; NaN ; NaN ];
Tc_13  = [ NaN ; NaN ; NaN ];
omc_error_13 = [ NaN ; NaN ; NaN ];
Tc_error_13  = [ NaN ; NaN ; NaN ];

%-- Image #14:
omc_14 = [ NaN ; NaN ; NaN ];
Tc_14  = [ NaN ; NaN ; NaN ];
omc_error_14 = [ NaN ; NaN ; NaN ];
Tc_error_14  = [ NaN ; NaN ; NaN ];

%-- Image #15:
omc_15 = [ NaN ; NaN ; NaN ];
Tc_15  = [ NaN ; NaN ; NaN ];
omc_error_15 = [ NaN ; NaN ; NaN ];
Tc_error_15  = [ NaN ; NaN ; NaN ];

%-- Image #16:
omc_16 = [ NaN ; NaN ; NaN ];
Tc_16  = [ NaN ; NaN ; NaN ];
omc_error_16 = [ NaN ; NaN ; NaN ];
Tc_error_16  = [ NaN ; NaN ; NaN ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ NaN ; NaN ; NaN ];
Tc_error_17  = [ NaN ; NaN ; NaN ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ NaN ; NaN ; NaN ];
Tc_error_18  = [ NaN ; NaN ; NaN ];

%-- Image #19:
omc_19 = [ NaN ; NaN ; NaN ];
Tc_19  = [ NaN ; NaN ; NaN ];
omc_error_19 = [ NaN ; NaN ; NaN ];
Tc_error_19  = [ NaN ; NaN ; NaN ];

%-- Image #20:
omc_20 = [ NaN ; NaN ; NaN ];
Tc_20  = [ NaN ; NaN ; NaN ];
omc_error_20 = [ NaN ; NaN ; NaN ];
Tc_error_20  = [ NaN ; NaN ; NaN ];

