slika = imread('Image15.tif');

slika = repmat(slika, [1, 1, 3]);
image(uint8(slika));

global stevilo_tock
stevilo_tock = 10;


disp("Insert outer points");
[u_outer, v_outer] = ginput(stevilo_tock);
global XY_outer
XY_outer = zeros(2, stevilo_tock);
XY_outer(1, (1:stevilo_tock))  = u_outer;
XY_outer(2, (1:stevilo_tock)) = v_outer;

disp("Insert inner points");
[u_inner, v_inner] = ginput(stevilo_tock);
global XY_inner
XY_inner = zeros(2, stevilo_tock);
XY_inner(1, (1:stevilo_tock))  = u_inner;
XY_inner(2, (1:stevilo_tock)) = v_inner;

rows = size(slika, 1);
columns = size(slika, 2);

corrected = zeros(rows, columns, 3);
f = 400;
dist_array = [ ];
f_array = [ ];

while (f < 801)
    XY_outer_ = zeros(2, stevilo_tock);
    XY_inner_ = zeros(2, stevilo_tock);
    
    for point_num=1:stevilo_tock
        X_ = XY_outer(1, point_num) - 320;
        Y_ = 241 - XY_outer(2, point_num);
        [theta,rho] = cart2pol(X_,Y_);
        rho_corrected = - (f/2) * (exp(-2*rho/f) -1)/(exp(-rho/f));
        [X, Y] = pol2cart(theta, rho_corrected);
        XY_outer_(1, point_num) = X;
        XY_outer_(2, point_num) = Y;
    end
    
    for point_num=1:stevilo_tock
        X_ = XY_inner(1, point_num) - 320;
        Y_ = 241 - XY_inner(2, point_num);
        [theta,rho] = cart2pol(X_,Y_);
        rho_corrected = - (f/2) * (exp(-2*rho/f) -1)/(exp(-rho/f));
        [X, Y] = pol2cart(theta, rho_corrected);
        XY_inner_(1, point_num) = X;
        XY_inner_(2, point_num) = Y;
    end
    
    [C, dist_outer] = fitline(XY_outer_);
    [C, dist_inner] = fitline(XY_inner_);
    
    dist = sum(dist_outer) + sum(dist_inner);
    f_array = [f_array f];
    dist_array = [dist_array dist];
    f = f + 3;
end

plot(f_array, dist_array);

min_dist = fminsearch(@funkcija, [500]);
disp(min_dist);


%% Funkcija za najdenje minimuma

function dist = funkcija(f_)
    f = f_(1);
    global stevilo_tock
    global XY_outer
    global XY_inner
    XY_outer_ = zeros(2, stevilo_tock);
    XY_inner_ = zeros(2, stevilo_tock);
    
    for point_num=1:stevilo_tock
        X_ = XY_outer(1, point_num) - 320;
        Y_ = 241 - XY_outer(2, point_num);
        [theta,rho] = cart2pol(X_,Y_);
        rho_corrected = - (f/2) * (exp(-2*rho/f) -1)/(exp(-rho/f));
        [X, Y] = pol2cart(theta, rho_corrected);
        XY_outer_(1, point_num) = X;
        XY_outer_(2, point_num) = Y;
    end
    
    for point_num=1:stevilo_tock
        X_ = XY_inner(1, point_num) - 320;
        Y_ = 241 - XY_inner(2, point_num);
        [theta,rho] = cart2pol(X_,Y_);
        rho_corrected = - (f/2) * (exp(-2*rho/f) -1)/(exp(-rho/f));
        [X, Y] = pol2cart(theta, rho_corrected);
        %X = int32(X);
        %Y = int32(Y);
        %column_ = X+320;
        %row_ = 240 - Y;
        XY_inner_(1, point_num) = X;
        XY_inner_(2, point_num) = Y;
    end
    
    [C, dist_outer] = fitline(XY_outer_);
    [C, dist_inner] = fitline(XY_inner_);
    
    dist = sum(dist_outer) + sum(dist_inner);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Za risanje slike
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%     for row=1:rows
%         for column=1:columns
%             X_ = column - 320;
%             Y_ = 241 - row;
%             [theta,rho] = cart2pol(X_,Y_);
%             rho_corrected = - (f/2) * (exp(-2*rho/f) -1)/(exp(-rho/f));
%             [X, Y] = pol2cart(theta, rho_corrected);
%             X = int32(X);
%             Y = int32(Y);
%             column_ = X+320;
%             row_ = 240 - Y;
%             if (row_ > 0 && row_ < 480 && column_ > 0 && column_ < 640) 
%                 corrected(row_, column_, (1:3)) =  slika(row, column, (1:3));
%             end
%         end
%     %disp(row);
%     end


