clc
clear

writerObj_video_evklidska_gray = VideoWriter('video_evklidska_gray.avi');
writerObj_video_evklidska_gray.FrameRate=24;

writerObj_video_evklidska_rgb = VideoWriter('video_evklidska_rgb.avi');
writerObj_video_evklidska_rgb.FrameRate=24;

writerObj_video_hi_gray = VideoWriter('video_hi_gray.avi');
writerObj_video_hi_gray.FrameRate=24;

writerObj_video_ucenje_gray = VideoWriter('video_ucenje_gray.avi');
writerObj_video_ucenje_gray.FrameRate=24;

open(writerObj_video_evklidska_gray);
open(writerObj_video_evklidska_rgb);
open(writerObj_video_ucenje_gray);
open(writerObj_video_hi_gray);

film = VideoReader('squash.avi');
video_rgb = read(film, [1,inf]);

video_gray = zeros(size(video_rgb,1),size(video_rgb,2),size(video_rgb,4),'uint8');
for f = 1:size(video_rgb,4)
    video_gray(:,:,f) = rgb2gray(video_rgb(:,:,:,f));
end
%%
implay(video_rgb, film.FrameRate);

%% ZAJEM IGRALCA
%%
figure;
imshow(video_rgb(:,:,:,1))
rect = uint64(ginput(2));

ref_gray = video_gray(:,:,1);
ref_gray = ref_gray(rect(1,2):rect(2,2),...
          rect(1,1):rect(2,1) );
ref_rgb = video_rgb(:,:,:,1);
ref_rgb = ref_rgb(rect(1,2):rect(2,2),...
          rect(1,1):rect(2,1), :);
      
w = rect(2,1) - rect(1,1) +1;
h = rect(2,2) - rect(1,2) +1;

imshow(ref_rgb)

%% SIVINSKA SLIKA - EVKLIDSKA RAZDALJA
%%
video_gray_evklidska = zeros(size(video_rgb),'uint8');

d = 5;  % velikost okolice za iskanje
x_ref = rect(1,1);
y_ref = rect(1,2);

for f = 1:size(video_gray,3)
    
    dist_min = Inf;
    fr = video_gray(y_ref-d:y_ref+h+d-2,...
                    x_ref-d:x_ref+w+d-2, f);
           
    for i = 1:size(fr,2) - w +1
        for j = 1:size(fr,1) - h +1
            
            target = fr(j:j+h-1,...
                        i:i+w-1 );
            dist = sqrt( sum(sum( (target-ref_gray).^2 )) );
            
            if dist < dist_min
                dist_min = dist;
                i_=i;
                j_=j;
            end
        end
    end
    x_ref = x_ref-d+i_-1;
    y_ref = y_ref-d+j_-1;
    if x_ref <= d
        x_ref = d+1;
    end
    if y_ref <= d
        y_ref = d+1;
    end
    
    video_gray_evklidska(:,:,:,f) = ...
        insertMarker(video_gray(:,:,f), uint64([x_ref+w/2, y_ref+h/2]));
    
    writeVideo(writerObj_video_evklidska_gray, video_gray_evklidska(:,:,:,f));
end
%%
% implay(video_gray_evklidska, film.FrameRate);

%% RGB SLIKA - EVKLIDSKA RAZDALJA
%%
video_rgb_evklidska = zeros(size(video_rgb),'uint8');

d = 5;  % velikost okolice za iskanje
x_ref = rect(1,1);
y_ref = rect(1,2);
%ref_ = rgb2hsv(ref_gray);
for f = 1:size(video_rgb,4)
    
    %for c = 1:3
    dist_min = Inf;
    fr = video_rgb(y_ref-d:y_ref+h+d-2,...
                    x_ref-d:x_ref+w+d-2, :,f);
    
    for i = 1:size(fr,2) - w +1
        for j = 1:size(fr,1) - h +1
            
            target = fr(j:j+h-1,...
                        i:i+w-1, :);
            dist = sqrt( sum(sum(sum( (target-ref_rgb).^2 ))) );
            
            if dist < dist_min
                dist_min = dist;
                i_=i;
                j_=j;
            end
        end
    end
    x_ref = x_ref-d+i_-1;
    y_ref = y_ref-d+j_-1;
    if x_ref <= d
        x_ref = d+1;
    end
    if y_ref <= d
        y_ref = d+1;
    end
    video_rgb_evklidska(:,:,:,f) = ...
        insertMarker(video_rgb(:,:,:,f), uint64([x_ref+w/2, y_ref+h/2]));
    
    writeVideo(writerObj_video_evklidska_rgb, video_rgb_evklidska(:,:,:,f));

end
%%
% implay(video_rgb_evklidska, film.FrameRate);

%% HISTOGRAMI - HI-KVADRAT
%%
video_gray_hi = zeros(size(video_rgb),'uint8');

n_bin = 20; %  �t. binov v histogramu
lut = idivide(ref_gray(:),256/n_bin)+1;
h_ref = zeros(n_bin,1);
for ii = 1:size(ref_gray(:),1)
    h_ref( lut(ii) ) = h_ref( lut(ii) ) + 1;
end

d = 5;  % velikost okolice za iskanje
x_ref = rect(1,1);
y_ref = rect(1,2);

for f = 1:size(video_rgb,4)
    
    hi_min = Inf;
    fr = video_gray(y_ref-d:y_ref+h+d-2,...
                    x_ref-d:x_ref+w+d-2, f);
    
    for i = 1:size(fr,2) - w +1
        for j = 1:size(fr,1) - h +1
            target = fr(j:j+h-1,...
                        i:i+w-1 );
            
            lut = idivide(target(:),256/n_bin)+1;
            h_tar = zeros(n_bin,1);
            for ii = 1:size(target(:),1)
                h_tar( lut(ii) ) = h_tar( lut(ii) ) + 1;
            end
            
            hi_kv = (( h_tar - h_ref ).^2)./( h_tar + h_ref) ;% ./ h_ref; <- �e delit z teor. vred.??->deljenje z 0!
            hi_kv(isnan(hi_kv))=0;
            hi_kv = 0.5*sum(hi_kv(:));
            %disp(min( h_tar + h_ref));
            
            if hi_kv < hi_min
                hi_min = hi_kv;
                i_=i;
                j_=j;
                
            end
        end
    end
    x_ref = x_ref-d+i_-1;
    y_ref = y_ref-d+j_-1;
    if x_ref <= d
        x_ref = d+1;
    end
    if y_ref <= d
        y_ref = d+1;
    end
    video_gray_hi(:,:,:,f) = ...
        insertMarker(video_gray(:,:,f), uint64([x_ref+w/2, y_ref+h/2]));
    
    writeVideo(writerObj_video_hi_gray, video_gray_hi(:,:,:,f));
end
%%
%implay(video_gray_hi, film.FrameRate);

%% SPROTNO U�ENJE
%%
video_gray_ucenje = zeros(size(video_rgb),'uint8');

n_bin = 20; %  �t. binov v histogramu
d = 5;  % velikost okolice za iskanje
x_ref = rect(1,1);
y_ref = rect(1,2);

for f = 1:size(video_rgb,4)
    
    if f == 1
        ref = ref_gray;
    else
        ref = ref_gray;
        target = fr(j_:j_+h-1,...
                    i_:i_+w-1 );
    end
    
    lut = idivide(ref(:),256/n_bin)+1;
    h_ref = zeros(n_bin,1);
    for ii = 1:size(ref(:),1)
        h_ref( lut(ii) ) = h_ref( lut(ii) ) + 1;
    end
    
    hi_min = Inf;
    fr = video_gray(y_ref-d:y_ref+h+d-2,...
                    x_ref-d:x_ref+w+d-2, f);
    
    for i = 1:size(fr,2) - w +1
        for j = 1:size(fr,1) - h +1
            target = fr(j:j+h-1,...
                        i:i+w-1 );
            
            lut = idivide(target(:),256/n_bin)+1;
            h_tar = zeros(n_bin,1);
            for ii = 1:size(target(:),1)
                h_tar( lut(ii) ) = h_tar( lut(ii) ) + 1;
            end
            
            hi_kv = ( h_tar - h_ref ).^2;% ./ h_ref; <- �e delit z teor. vred.??->deljenje z 0!
            hi_kv = sum(hi_kv(:));
            
            if hi_kv < hi_min
                hi_min = hi_kv;
                i_=i;
                j_=j;
                
            end
        end
    end
    x_ref = x_ref-d+i_-1;
    y_ref = y_ref-d+j_-1;
    if x_ref <= d
        x_ref = d+1;
    end
    if y_ref <= d
        y_ref = d+1;
    end
    video_gray_ucenje(:,:,:,f) = ...
        insertMarker(video_gray(:,:,f), uint64([x_ref+w/2, y_ref+h/2]));
    
    writeVideo(writerObj_video_ucenje_gray, video_gray_ucenje(:,:,:,f));
end
%%
%implay(video_gray_ucenje, film.FrameRate);


%%

close(writerObj_video_evklidska_gray);
close(writerObj_video_evklidska_rgb);
close(writerObj_video_ucenje_gray);
close(writerObj_video_hi_gray);