penciles = imread('coloured_pencils.jpg');

penciles_hsv = rgb2hsv(penciles);

%disp(colormap(gray));

penciles_h = penciles_hsv(:, :, 1);
penciles_h = repmat(penciles_h, [1, 1, 3]);

penciles_s = penciles_hsv(:, :, 2);
penciles_s = repmat(penciles_s, [1, 1, 3]);

penciles_v = penciles_hsv(:, :, 3);
penciles_v = repmat(penciles_v, [1, 1, 3]);


yosemite = imread('yosemite_meadows.jpg');

yosemite_hsv = rgb2hsv(yosemite);

yosemite_h = yosemite_hsv(:, :, 1);
yosemite_h = repmat(yosemite_h, [1, 1, 3]);

yosemite_s = yosemite_hsv(:, :, 2);
yosemite_s = repmat(yosemite_s, [1, 1, 3]);

yosemite_v = yosemite_hsv(:, :, 3);
yosemite_v = repmat(yosemite_v, [1, 1, 3]);

subplot(2, 3, 1), imshow(uint8(penciles_h), [0, 100])
subplot(2, 3, 2), image(uint8(penciles_s))
subplot(2, 3, 3), image(uint8(penciles_v))

subplot(2, 3, 4), image(uint8(yosemite_h))
subplot(2, 3, 5), image(uint8(yosemite_s))
subplot(2, 3, 6), image(uint8(yosemite_v))
pause;
close all


penciles_lab = rgb2lab(penciles);

penciles_l = penciles_lab(:, :, 1);
penciles_l = repmat(penciles_l, [1, 1, 3]);

penciles_a = penciles_lab(:, :, 2);
penciles_a = repmat(penciles_a, [1, 1, 3]);

penciles_b = penciles_lab(:, :, 3);
penciles_b = repmat(penciles_b, [1, 1, 3]);

yosemite_lab = rgb2lab(yosemite);

yosemite_l = yosemite_lab(:, :, 1);
yosemite_l = repmat(yosemite_l, [1, 1, 3]);

yosemite_a = yosemite_lab(:, :, 2);
yosemite_a = repmat(yosemite_a, [1, 1, 3]);

yosemite_b = yosemite_lab(:, :, 3);
yosemite_b = repmat(yosemite_b, [1, 1, 3]);

subplot(2, 3, 1), image(uint8(penciles_l))
subplot(2, 3, 2), image(uint8(penciles_a))
subplot(2, 3, 3), image(uint8(penciles_b))

subplot(2, 3, 4), image(uint8(yosemite_l))
subplot(2, 3, 5), image(uint8(yosemite_a))
subplot(2, 3, 6), image(uint8(yosemite_b))

pause;


