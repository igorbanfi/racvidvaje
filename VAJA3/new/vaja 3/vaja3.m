clc
clear
close all

narava = imread('narava.jpg');
barvice = imread('barvice.jpg');

film = VideoReader('ladja.avi');
video = read(film, [1,inf]);

video_hsv = zeros(size(video));
for f = 1:size(video,4)
    video_hsv(:,:,:,f) = rgb2hsv(video(:,:,:,f));
end

%% 1 komponente HSV in LAB
%%
komponente(narava);
komponente(barvice);

%% 2 Vpliv posameznih komponent na rekonstruirano RGB sliko
%%
vpliv_komponent(narava);
vpliv_komponent(barvice);

%% 3 Ro�na segmentacija na podlagi barve
%%
frame = video(:,:,:,20);
% komponente(frame);
% colorThresholder(frame); % orodje

rgb = [25 125; 50 150; 25 150];
hsv = [0.25 1; 0.2 1; 0.1 0.6];

video_1_rgb = tresholder(video    ,rgb);
video_1_hsv = tresholder(video_hsv,hsv);

video_1_hsv = uint8(video_1_hsv);

implay(video_1_rgb, film.FrameRate);
implay(video_1_hsv, film.FrameRate);

%% 4 Avtomatska segmentacija na podlagi barve
%%
frame = video(:,:,:,20);
figure;
imshow(frame); hold on;
% say('Please select region of interest');

input = ginput;
plot(input([1:end,1],1), input([1:end,1],2),'r-','linewidth',2);
    
mask = uint8( roipoly( frame, input(:,1), input(:,2) ));
pts1D = zeros(sum(mask(:)==1),3,'uint8');
for i = 1:3
    c = frame(:,:,i);
    pts1D(:,i) = c(mask(:)==1); 
end

figure;
scatter3(pts1D(:,1),pts1D(:,2),pts1D(:,3),1,pts1D/255)

%%
n_bin = 8;
d = 256/n_bin;
lut = idivide(pts1D,d)+1;

h_ref = zeros(n_bin,n_bin,n_bin);

for i = 1:size(pts1D,1)
    h_ref( lut(i,1),lut(i,2),lut(i,3) ) = ...
        h_ref( lut(i,1),lut(i,2),lut(i,3) ) + 1;
end

h_ref = uint8(255*h_ref/max(max(max(h_ref))));

video_2 = zeros(size(video(:,:,1,:)),'uint8');

for f = 1:size(video,4)
    fr = video(:,:,:,f);
    fr = idivide(fr,d)+1;
    
    for i = 1:size(frame,1)
        for j = 1:size(frame,2)
            video_2(i,j,1,f) = h_ref( fr(i,j,1),fr(i,j,2),fr(i,j,3) );
        end
    end
end
video_2 = repmat(video_2,[1,1,3,1]);
implay(video_2, film.FrameRate);

%% 5 Segmentacija na podlagi razdalje med histogrami
%%
n_ele = 8;
n_bin = 4;

d = 256/n_bin;
lut = idivide(pts1D,d)+1;
h_ref = zeros(n_bin,n_bin,n_bin);
for i = 1:size(pts1D,1)
    h_ref( lut(i,1),lut(i,2),lut(i,3) ) = ...
        h_ref( lut(i,1),lut(i,2),lut(i,3) ) + 1;
end
h_ref = h_ref/sum(sum(sum(h_ref)));

%%
x_seg = floor(size(fr,1)/n_ele)-1;
y_seg = floor(size(fr,2)/n_ele)-1;
video_3 = zeros(size(video),'uint8');

for f = 1:size(video,4)
    
    fr = idivide(video(:,:,:,f),d)+1;
    fr_new = zeros(size(fr),'uint8');

    for x = n_ele*(0:x_seg)
        for y = n_ele*(0:y_seg)
            % (x,y)-ti segment slike
            h_seg = zeros(n_bin,n_bin,n_bin);

            for xx = x+(1:n_ele)
            for yy = y+(1:n_ele)

                h_seg( fr(xx,yy,1),fr(xx,yy,2),fr(xx,yy,3) ) = ...
                    h_seg( fr(xx,yy,1),fr(xx,yy,2),fr(xx,yy,3) ) + 1;
            end
            end
            h_seg = h_seg/n_ele^2; % preveri sum == 1
            match = sum(sum(sum(  min(h_ref,h_seg) )));
            fr_new( x+(1:n_ele), y+(1:n_ele), : ) = uint8(255*match);
        end
    end
    video_3(:,:,:,f) = fr_new;
end

implay(video_3, film.FrameRate);

%% zapis
writer = VideoWriter('ladja 2.avi');
writer.FrameRate = film.FrameRate;
open(writer);
writeVideo(writer, video_2);
close(writer);

%% funkcije
function say(text)
NET.addAssembly('System.Speech');
obj = System.Speech.Synthesis.SpeechSynthesizer;
obj.Volume = 100;
Speak(obj, text );
end

function komponente(slika_rgb)

slika_hsv  = rgb2hsv(slika_rgb);
slika_lab  = rgb2lab(slika_rgb);
% HSV
figure;
subplot(2,2,1); title('H component'); hold on;
imshow(slika_hsv(:,:,1),[],'InitialMagnification','fit');
subplot(2,2,2); title('S component'); hold on;
imshow(slika_hsv(:,:,2),[],'InitialMagnification','fit');
subplot(2,2,3); title('V component'); hold on;
imshow(slika_hsv(:,:,3),[],'InitialMagnification','fit');
subplot(2,2,4); title('RGB'); hold on;
imshow(slika_rgb,'InitialMagnification','fit');
% LAB
figure;
subplot(2,2,1); title('L component'); hold on;
imshow(slika_lab(:,:,1),[],'InitialMagnification','fit');
subplot(2,2,2); title('A component'); hold on;
imshow(slika_lab(:,:,2),[],'InitialMagnification','fit');
subplot(2,2,3); title('B component'); hold on;
imshow(slika_lab(:,:,3),[],'InitialMagnification','fit');
subplot(2,2,4); title('RGB'); hold on;
imshow(slika_rgb,'InitialMagnification','fit');
hold off;
end

function vpliv_komponent(slika_rgb)

slika_hsv = rgb2hsv(slika_rgb);
slika_lab = rgb2lab(slika_rgb);
% HSV
slika_h = slika_hsv;
slika_h(:,:,1)  = mean(mean( slika_hsv(:,:,1) ));
slika_h = hsv2rgb(slika_h);
slika_s = slika_hsv;
slika_s(:,:,2)  = mean(mean( slika_hsv(:,:,2) ));
slika_s = hsv2rgb(slika_s);
slika_v = slika_hsv;
slika_v(:,:,3)  = mean(mean( slika_hsv(:,:,3) ));
slika_v = hsv2rgb(slika_v);
%LAB
slika_l = slika_lab;
slika_l(:,:,1)  = mean(mean( slika_lab(:,:,1) ));
slika_l = lab2rgb(slika_l);
slika_a = slika_lab;
slika_a(:,:,2)  = mean(mean( slika_lab(:,:,2) ));
slika_a = lab2rgb(slika_a);
slika_b = slika_lab;
slika_b(:,:,3)  = mean(mean( slika_lab(:,:,3) ));
slika_b = lab2rgb(slika_b);
%HSV
figure;
subplot(2,2,1); title('h comp avrage'); hold on;
imshow(slika_h,'InitialMagnification','fit');
subplot(2,2,2); title('s comp avrage'); hold on;
imshow(slika_s,'InitialMagnification','fit');
subplot(2,2,3); title('v comp avrage'); hold on;
imshow(slika_v,'InitialMagnification','fit');
subplot(2,2,4); title('RGB'); hold on;
imshow(slika_rgb,'InitialMagnification','fit');
%LAB
figure;
subplot(2,2,1); title('L comp avrage'); hold on;
imshow(slika_l,'InitialMagnification','fit');
subplot(2,2,2); title('A comp avrage'); hold on;
imshow(slika_a,'InitialMagnification','fit');
subplot(2,2,3); title('B comp avrage'); hold on;
imshow(slika_b,'InitialMagnification','fit');
subplot(2,2,4); title('RGB'); hold on;
imshow(slika_rgb,'InitialMagnification','fit');
hold off;
end

function video_out = tresholder(video,rgb)
    video_out = zeros(size(video),'like',video);
    for f = 1:size(video,4)
        mask = ones(size(video(:,:,1,1)));
        fr = video(:,:,:,f);
        for i = 1:3
            c = fr(:,:,i);
            mask(c<rgb(i,1)) = 0;
            mask(c>rgb(i,2)) = 0;
            mask(c<rgb(i,1)) = 0;
            mask(c>rgb(i,2)) = 0;
            mask(c<rgb(i,1)) = 0;
            mask(c>rgb(i,2) )= 0;
        end
        video_out(:,:,:,f) = repmat(255*mask,[1,1,3]);
        %bsxfun(@times, fr, cast(mask, 'like', frame));
    end
end

