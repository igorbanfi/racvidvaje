lena = imread('lena.tiff');
baboon = imread('baboon.tiff');

hollywood = VideoReader('Holywood2-t00427-rgb.avi');
hollywoodFrames = read(hollywood, [1, inf]);

fprintf('NALOGA 1 \n \n');

fprintf("Vrednosti slikovnega elementa vrstica 350, stolpec 100: \n");

fprintf("\n LENA \n \n");
fprintf('R: %d,  ', lena(350, 100, 1));
fprintf('G: %d,  ', lena(350, 100, 2));
fprintf('B: %d \n', lena(350, 100, 3));

fprintf('\nBABOON\n');

fprintf('R: %d,  ', baboon(350, 100, 1));
fprintf('G: %d,  ', baboon(350, 100, 2));
fprintf('B: %d \n', baboon(350, 100, 3));

fprintf('\nHOLLYWOOD\n');
fprintf('Vrednost RGB v 100 stolpcu in 100 vrstici \n \n');

for frame=50:60
    fprintf('frameNum: %d \n', frame);
    fprintf('R: %d,  ', hollywoodFrames(100, 100, 1, frame));
    fprintf('G: %d,  ', hollywoodFrames(100, 100, 2, frame));
    fprintf('B: %d \n', hollywoodFrames(100, 100, 3, frame));
end

pause;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NALOGA 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%
% LENA
%%%

RGB_to_gray_vec = single([0.299 0.587 0.114]);
rows_lena = size(lena, 1);
columns_lena = size(lena, 2);

grey_lena = zeros(rows_lena, columns_lena, 'double');
for row=1:rows_lena
    for column=1:columns_lena
        RGB_lena = single(lena(row, column, 1:3));
        RGB = [RGB_lena(1) RGB_lena(2) RGB_lena(3)];
        grey_lena(row, column) =  dot(RGB,RGB_to_gray_vec);
    end
end

grey_lena = repmat(grey_lena, [1, 1, 3]);
imwrite(uint8(grey_lena), "lena_gray.tiff");
image(uint8(grey_lena));

pause;

%%%
% Baboon
%%%

RGB_to_gray_vec = single([0.299 0.587 0.114]);
rows_baboon = size(baboon, 1);
columns_baboon = size(baboon, 2);

grey_baboon = zeros(rows_baboon, columns_baboon, 'double');
for row=1:rows_baboon
    for column=1:columns_baboon
        RGB_baboon = single(baboon(row, column, 1:3));
        RGB = [RGB_baboon(1) RGB_baboon(2) RGB_baboon(3)];
        grey_baboon(row, column) =  dot(RGB,RGB_to_gray_vec);
    end
end

grey_baboon = repmat(grey_baboon, [1, 1, 3]);
imwrite(uint8(grey_baboon), "baboon_gray.tiff");
image(uint8(grey_baboon));

pause;

%%%
% Holywood
%%%

disp(size(hollywoodFrames));
RGB_to_gray_vec = single([0.299 0.587 0.114]);
rows_hollywoodFrames = size(hollywoodFrames, 1);
columns_hollywoodFrames = size(hollywoodFrames, 2);
frames_hollywoodFrames = size(hollywoodFrames, 4);

grey_hollywoodFrame = zeros(rows_hollywoodFrames, columns_hollywoodFrames, 'double');
%grey_hollywoodFrame = zeros(

writerObj = VideoWriter('gray_1.avi');
writerObj.FrameRate = 24;

open(writerObj);


for frame=1:frames_hollywoodFrames
%for frame=1:30
    grey_hollywoodFrame = zeros(rows_hollywoodFrames, columns_hollywoodFrames, 'double');
    for column=1:columns_hollywoodFrames
        for row=1:rows_hollywoodFrames
            RGB_hollywood = single(hollywoodFrames(row, column, 1:3, frame));
            RGB = [RGB_hollywood(1) RGB_hollywood(2) RGB_hollywood(3)];
            grey_hollywoodFrame(row, column) =  dot(RGB, RGB_to_gray_vec);
        end
    end
    grey_hollywoodFrame = repmat(grey_hollywoodFrame, [1, 1, 3]);
    writeVideo(writerObj, uint8(grey_hollywoodFrame));
    disp(frame);
end

close(writerObj);

fprintf("Done graying video");
pause;

%%%%%%%%%%%%%
% NALOGA 3
%%%%%%%%%%%%%


%%%
% LENA
%%%

rows_lena = size(lena, 1);
columns_lena = size(lena, 2);

lena_3 = zeros(rows_lena, columns_lena, 3, 'double');
for row=1:rows_lena
    for column=1:columns_lena
        RGB_lena = single(grey_lena(row, column, 1:3));
        RGB = [RGB_lena(1) RGB_lena(2) RGB_lena(3)];
        if RGB(1) < 100
            RGB_3 = [0 255 0];
        elseif RGB(1) < 200
            RGB_3 = [255 191 0];
        else
            RGB_3 = [255 0 0];
        end
        
        lena_3(row, column, 1:3) = RGB_3;
    end
end

imwrite(uint8(lena_3), "lena_3.tiff");
image(uint8(lena_3));

pause;

%%%
% Baboon
%%%

rows_baboon = size(baboon, 1);
columns_baboon = size(baboon, 2);

baboon_3 = zeros(rows_baboon, columns_baboon, 3, 'double');
for row=1:rows_baboon
    for column=1:columns_baboon
        RGB_baboon = single(grey_baboon(row, column, 1:3));
        RGB = [RGB_baboon(1) RGB_baboon(2) RGB_baboon(3)];
        if RGB(1) < 100
            RGB_3 = [0 255 0];
        elseif RGB(1) < 200
            RGB_3 = [255 191 0];
        else
            RGB_3 = [255 0 0];
        end
        baboon_3(row, column, 1:3) = RGB_3;
    end
end

imwrite(uint8(baboon_3), "baboon_3.tiff");
image(uint8(baboon_3));

pause;

%%%
% Holywood
%%%

hollywoodGray = VideoReader('gray.avi');
hollywoodFramesGray = read(hollywoodGray, [1, inf]);

rows_hollywoodFrames = size(hollywoodFrames, 1);
columns_hollywoodFrames = size(hollywoodFrames, 2);
frames_hollywoodFrames = size(hollywoodFrames, 4);

hollywoodFrame_3 = zeros(rows_hollywoodFrames, columns_hollywoodFrames, 'double');

writerObj = VideoWriter('hollywood_3_new.avi');
writerObj.FrameRate = 24;

open(writerObj);


%for frame=1:frames_hollywoodFrames
for frame=1:30
    hollywoodFrame_3 = zeros(rows_hollywoodFrames, columns_hollywoodFrames, 3,'double');
    for column=1:columns_hollywoodFrames
        for row=1:rows_hollywoodFrames
            RGB_hollywood = single(hollywoodFramesGray(row, column, 1:3, frame));
            RGB = [RGB_hollywood(1) RGB_hollywood(2) RGB_hollywood(3)];
            if RGB(1) < 100
                RGB_3 = [0 255 0];
            elseif RGB(1) < 200
                RGB_3 = [255 191 0];
            else
                RGB_3 = [255 0 0];
            end
            hollywoodFrame_3(row, column, 1:3) = RGB_3;
        end
    end
    writeVideo(writerObj, uint8(hollywoodFrame_3));
    disp(frame);
end

close(writerObj);

fprintf("DONE\n");





