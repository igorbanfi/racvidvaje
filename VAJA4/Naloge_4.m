video = VideoReader('squash.avi');
frames = read(video,[1 Inf]);

ozadje_mean = imread('ozadje_mean.png');

%% Naloga 4

writerObj = VideoWriter('Naloga_4.avi');
writerObj.FrameRate=24;

open(writerObj);

% priprava spremenljivk
sample_frames = frames;
sample_frames_size = size(sample_frames);
sample_frames_razlika = sample_frames;
sample_frames_binary = zeros(sample_frames_size(1), sample_frames_size(2), sample_frames_size(4));

%iteriramo po framig
for frame = 1:sample_frames_size(4)
    % izračunamo razliko
    sample_frames_razlika(:, :, :, frame) = sample_frames(:, :, :, frame)-ozadje_mean;
    
    % binariziramo sliko
    sample_frames_binary(:, :, frame) = imbinarize(rgb2gray(sample_frames_razlika(:, :, :, frame)),...
                                                                            5./255.);
    
    % morfološke operacije               
    sample_frames_binary(:, :, frame) = bwmorph(sample_frames_binary(:, :, frame), 'open', 5);

    % najdemo 2 največja objekra
    sample_frames_binary(:, :, frame) = bwareafilt(logical(sample_frames_binary(:, :, frame)), 2);
    
    % vsakemu damo svojo vrednost
    sample_frames_binary(:, :, frame) = bwlabel(sample_frames_binary(:, :, frame));
    
    % izračunamo težišče posameznega objekta
    s = regionprops(sample_frames_binary(:, :, frame),'centroid');
    centroids = cat(1,s.Centroid);
    
    % narišemo težiščni oznaki na frame
    sample_frames(:, :, :, frame) = insertMarker(sample_frames(:, :, :, frame), centroids);
    
    % frame zapišemo v video
    writeVideo(writerObj, sample_frames(:, :, :, frame));
    
    disp(frame);
end

close(writerObj);
