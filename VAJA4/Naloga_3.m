video = VideoReader('squash.avi');
frames = read(video,[1 Inf]);

%% Naloga 3

sample_frames = frames(:, :, :, 1:20:end);

frame_size = size(frames);

ozadje_median = zeros(frame_size(1), frame_size(2), frame_size(3));
ozadje_mean = zeros(frame_size(1), frame_size(2), frame_size(3));

 for u = 1:frame_size(2)
    for v = 1:frame_size(1)
        for channel = 1:frame_size(3)
            ozadje_median(v, u, channel) = median(sample_frames(v, u, channel, :));
        end
    end
end

for u = 1:frame_size(2)
    for v = 1:frame_size(1)
        for channel = 1:frame_size(3)
            ozadje_mean(v, u, channel) = trimmean(sample_frames(v, u, channel, :), 30);
        end
    end
 end
ozadje_median = uint8(ozadje_median);
ozadje_mean = uint8(ozadje_mean);

imwrite(ozadje_median, 'ozadje_median.png');
imwrite(ozadje_mean, 'ozadje_mean.png');

imshowpair(ozadje_median, ozadje_mean, 'montage'), hold on

