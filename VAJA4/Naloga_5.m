video = VideoReader('squash.avi');
frames = read(video,[1 Inf]);

ozadje_mean = imread('ozadje_mean.png');

%% Naloga 5

writerObj = VideoWriter('Naloga_5.avi');
writerObj.FrameRate=24;

open(writerObj);

% pripravimo spremenljivke
sample_frames = read(video,[1 Inf]);
sample_frames_size = size(sample_frames);
sample_frames_razlika = sample_frames;
sample_frames_razlika_red = zeros(size(sample_frames_razlika));
sample_frames_razlika_white = zeros(size(sample_frames_razlika));
sample_frames_binary = zeros(sample_frames_size(1), sample_frames_size(2), sample_frames_size(4));
sample_frames_binary_red = sample_frames_binary;
sample_frames_binary_white = sample_frames_binary;

% iteriramo po framih
for frame = 1:sample_frames_size(4)
    % izračunamo razliko
    sample_frames_razlika(:, :, :, frame) = sample_frames(:, :, :, frame)-ozadje_mean;
    
    % naredimo masko čez slike za rdečo in belo barvo
    sample_frame_red_ = thresholderRed(sample_frames(:, :, :, frame));
    sample_frame_white_ = thresholderWhite(sample_frames(:, :, :, frame));
    
    % množimo razliko in masko, ter sliki normaliziramo
    sample_frame_red = uint8(rgb2gray(sample_frames_razlika(:, :, :, frame))).*uint8(sample_frame_red_);
    sample_frame_red = uint8(double(sample_frame_red)./double(max(max(sample_frame_red)))*255.);
    
    sample_frame_white = uint8(rgb2gray(sample_frames_razlika(:, :, :, frame))).*uint8(sample_frame_white_);
    sample_frame_white = uint8(double(sample_frame_white)./double(max(max(sample_frame_white)))*255.);
    
    % binariziramo sliko za zaznavanje belega igralca
    sample_frames_binary_white(:, :, frame) = imbinarize(sample_frame_white, 0.02);
    
    % izvedemo morfološke operacije
    sample_frames_binary_white(:, :, frame) = bwmorph(sample_frames_binary_white(:, :, frame), 'clean', 1);
    
    sample_frames_binary_white(:, :, frame) = bwmorph(sample_frames_binary_white(:, :, frame), 'open', inf);
     
    sample_frames_binary_white(:, :, frame) = bwmorph(sample_frames_binary_white(:, :, frame), 'thin', 1);
    
    sample_frames_binary_white(:, :, frame) = bwmorph(sample_frames_binary_white(:, :, frame), 'majority', 5);

     
    se = strel('rectangle', [5, 5]);
    sample_frames_binary_white(:, :, frame) = imdilate(sample_frames_binary_white(:, :, frame), se);
    sample_frames_binary_white(:, :, frame) = bwmorph(sample_frames_binary_white(:, :, frame), 'majority', 30);

    % izločimo samo segment z največjo površino    
    sample_frames_binary_white(:, :, frame) = bwareafilt(logical(sample_frames_binary_white(:, :, frame)), 1);
    sample_frames_binary_white(:, :, frame) = bwlabel(sample_frames_binary_white(:, :, frame));
    
    % binariziramo sliko za zaznavanje rdečega igralca
    sample_frames_binary_red(:, :, frame) = imbinarize(sample_frame_red, 0.015);
    
    % izvedemo morfološke operacije
    sample_frames_binary_red(:, :, frame) = bwmorph(sample_frames_binary_red(:, :, frame), 'majority', 10);
    
    % izločimo samo segment z največjo površino
    sample_frames_binary_red(:, :, frame) = bwareafilt(logical(sample_frames_binary_red(:, :, frame)), 1);
    sample_frames_binary_red(:, :, frame) = bwlabel(sample_frames_binary_red(:, :, frame));
    
    % določimo težišče belega igralca
    s_white = regionprops(sample_frames_binary_white(:, :, frame),'centroid');
    centroids_white = cat(1,s_white.Centroid);
    
    % določimo težišče rdečega igralca
    s_red = regionprops(sample_frames_binary_red(:, :, frame),'centroid');
    centroids_red = cat(1,s_red.Centroid);

    % v sliko vstavimo označbi za igralca
    sample_frames(:, :, :, frame) = insertText(sample_frames(:, :, :, frame), centroids_white, 'A');
    sample_frames(:, :, :, frame) = insertText(sample_frames(:, :, :, frame), centroids_red, 'B');
    
    % zapišemo frame
    writeVideo(writerObj, sample_frames(:, :, :, frame));
    
    disp(frame);
end

close(writerObj);
