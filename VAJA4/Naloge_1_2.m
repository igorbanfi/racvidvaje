clc
clear
close all

hotel = imread('hotel.jpg');

imshow(hotel,'InitialMagnification','fit');

%% Sobel

Kernel_sobel_x = [-1, 0, +1;
                    -2, 0, 2;
                    -1, 0, 1];
Kernel_sobel_y = [1, 2, 1;
                    0, 0, 0; 
                    -1, -2, -1];

Gx = imfilter(hotel, Kernel_sobel_x, 'conv');
Gy = imfilter(hotel, Kernel_sobel_y, 'conv');
Gx = double(Gx);
Gy = double(Gy);

G = sqrt(Gx.^2 + Gy.^2);
G = uint8(G);
G = rgb2gray(G);

G_new = zeros(size(G, 1), size(G, 2));
treshold = 100;
for v = 1:size(G,1)
    for u = 1:size(G, 2)
        if G(v, u, 1) > treshold
            G_new(v, u) = 1;
        else
            G_new(v, u) = 0;
        end
    end
end

Canny = edge(rgb2gray(hotel), 'Canny');

imshowpair(G_new, Canny, 'montage'), hold on;
hold off;

%% HUGH TRANSFORM


Hough_sobel = HoughTransform(G_new);
Hough_canny = HoughTransform(Canny);

figure, imshowpair(Hough_sobel, Hough_canny, 'montage'), hold on;
hold off;

DrawLines(hotel, Hough_sobel, 5);
%DrawLines(hotel, Hough_canny, 50);


%% Funkcije

function DrawLines(slika, Hough, N)
    n = linspace(-2000, 1999, 4000);
    k = linspace(-2, 20, 4000);
    n = reshape(n, [4000, 1]);
    k = reshape(k, [4000, 1]);
    n_ = [];
    k_ = [];
    
    for i=1:N
        maximum = max(max(Hough));
        [x, y] = find(Hough==maximum);
        Hough(x(1), y(1)) = 0;
        n_ = [n_, n(x(1))];
        k_ = [k_, k(y(1))];
    end
    n_ = reshape(n_, [N, 1]);
    k_ = reshape(k_, [N, 1]);
    
    Points_1 = [];
    Points_2 = [];
    for i=1:(size(n_))
        disp(i);
        n__ = n_(i);
        k__ = k_(i);
        x_1 = 0;
        y_1 = n__;
        x_2 = 480;
        y_2 = n__ + k__*x_2;
        Points_1 = [Points_1; x_1, y_1];
        Points_2 = [Points_2; x_2, y_2];
    end
    
    figure, imshow(slika), hold on
    
    for i=1:N
        plot([Points_1(i, 1), Points_2(i, 1)], ...
            [Points_1(i, 2), Points_2(i, 2)], ...
            'Color', 'r', 'LineWidth', 2)
        hold on
    end
    
end

function Hough = HoughTransform(EdgeMatrix)
    n = linspace(-2000, 1999, 4000);
    k = linspace(-20, 20, 4000);
    n = reshape(n, [4000, 1]);
    k = reshape(k, [4000, 1]);
    Hough = zeros(4000, 4000);
    for v = 1:size(EdgeMatrix,1)
        for u = 1:size(EdgeMatrix,2)
            if EdgeMatrix(v, u)
                for k_ind = 1:size(k)
                    n = double(v) - k(k_ind)*double(u);
                    n = int16(n);
                    if abs(n)<2000
                        n = n + 2000;
                        Hough(int16(n), k_ind) =   Hough(int16(n), k_ind) +1;
                    end
                end
            end
        end
    end
end



